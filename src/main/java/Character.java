import java.util.Random;

public abstract class Character extends GameObject{

    public Character(String filename, int posX, int posY) {
        super(filename, posX, posY);
    }

    int maxHp;
    int hP;
    int defendP;
    int strikeP;
    int level;

    public int randomizeD6(int sides){
        int d6;
        Random r = new Random();
        return d6 = r.nextInt(sides) + 1;
    }

    public boolean isAlive(){
        boolean isALive = true;
        if (hP <= 0){
            isALive = false;
        }
        return isALive;
    }

    public void strike (Character character){
        int strikeV = 2 * randomizeD6(6) + strikeP;
        if (strikeV > character.defendP){
            character.hP = character.hP - (strikeV - character.defendP);
        }
    }

    public void sethP(int hP) {
        this.hP = hP;
    }
}
