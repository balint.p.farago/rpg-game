import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class Board extends JComponent implements KeyListener{

    static Hero myHero = new Hero(0,0, 1);

    Skeleton skeleton1 = new Skeleton(4,4, 5);
    Skeleton skeleton2 = new Skeleton(6,3, 5);
    Skeleton skeleton3 = new Skeleton(2,6, 5);


    Boss myBoss = new Boss(2, 9, 1);


    //All method implementations that implemented from KeyListener interface

    @Override
    public void keyTyped(KeyEvent e) {
        //I don't need this
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_LEFT){
            myHero.move(-1, 0, board);
            loadImage("hero-left.png");
        }
        else if(key == KeyEvent.VK_RIGHT){
            myHero.move(1, 0, board);
            loadImage("hero-right.png");
        }
        else if(key == KeyEvent.VK_UP){
            myHero.move(0, -1, board);
            loadImage("hero-up.png");
        }
        else if(key == KeyEvent.VK_DOWN){
            myHero.move(0, 1, board);
            loadImage("hero-down.png");
        }
        else if (key == KeyEvent.VK_SPACE){
            if (isOnTheSameField(skeleton1)){
                battle(skeleton1);
            }
            else if (isOnTheSameField(skeleton2)){
                battle(skeleton2);
            }
            else if (isOnTheSameField(skeleton3)){
                battle(skeleton3);
            }
            else if (isOnTheSameField(myBoss)){
                battle(myBoss);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //I don't need this
    }

    //write the 'pattern' of your board
    // e.g.
    //0 = wall
    //1 = floor

    int [][] board = new int[][]{
        {1, 1, 1, 0, 1, 0, 1, 1, 1, 1},
        {1, 1, 1, 0, 1, 0, 1, 0, 0, 1},
        {1, 0, 0, 0, 1, 0, 1, 0, 0, 1},
        {1, 1, 1, 1, 1, 0, 1, 1, 1, 1},
        {0, 0, 0, 0, 1, 0, 0, 1, 1, 0},
        {1, 0, 1, 0, 1, 1, 1, 1, 0, 1},
        {1, 0, 1, 0, 1, 0, 0, 1, 0, 1},
        {1, 1, 1, 1, 1, 0, 0, 1, 0, 1},
        {1, 0, 0, 0, 1, 1, 1, 1, 0, 1},
        {1, 1, 1, 0, 1, 0, 0, 1, 0, 1}
    };
    ArrayList<GameObject> gameObjects = new ArrayList<>();

    public Board() {


        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[j][i] == 0) {
                    gameObjects.add(new Wall(i, j));
                } else {
                    gameObjects.add(new Floor(i, j));
                }
            }
        }
        // set the size of your draw board
        setPreferredSize(new Dimension(600, 600));
        setVisible(true);

    }

    public void loadImage(String filename){
        myHero.setImage(filename);
    }

    public boolean isOnTheSameField (Character character){
        boolean isOnTheSameField = false;
        if (myHero.posX == character.posX && myHero.posY == character.posY
        ){
            isOnTheSameField = true;
        }
        return isOnTheSameField;
    }


    public void battle (Character character){
        while (myHero.hP > 0 && character.hP > 0 && myHero.posX == character.posX && myHero.posY == character.posY){
            myHero.strike(character);
            if (character.hP > 0){
                try{
                    character.strike(myHero);
                    Thread.sleep(1500);
                }catch(InterruptedException ex){
                    System.out.println(ex);
                }

            }
        }
    }

    @Override
    public void paint(Graphics graphics) {
        // here you have a 600x600 canvas
        // you can create and draw an image using the GameObject class below
        // e.g.
        for (GameObject gameObject : gameObjects) {
            gameObject.draw(graphics);
        }
        // After painting the board, I can add the characters on it

        if (skeleton1.isAlive()) {
            skeleton1.draw(graphics);
        }
        if (skeleton2.isAlive()) {
            skeleton2.draw(graphics);
        }
        if (skeleton3.isAlive()) {
            skeleton3.draw(graphics);
        }
        if (myBoss.isAlive()) {
            myBoss.draw(graphics);
        }
        if (myHero.isAlive()){
            myHero.draw(graphics);
        }
        if (myHero.isAlive()) {
            graphics.drawString("Hero (level" + myHero.level + "):  HP: " + myHero.hP + "/" + myHero.maxHp + ", SP: " + myHero.strikeP + ", DP: " + myHero.defendP, 10, 620);
        }

        if (isOnTheSameField(myBoss) && myBoss.isAlive() && myHero.isAlive()){
            graphics.drawString("Boss (level" + myBoss.level +  "):  HP: " + myBoss.hP + "/" + myBoss.maxHp + ", SP: " + myBoss.strikeP + ", DP: " + myBoss.defendP, 310, 620);
        }
        if (isOnTheSameField(skeleton1) && skeleton1.isAlive() && myHero.isAlive()){
            graphics.drawString("Skeleton (level" + skeleton1.level +  "):  HP: " + skeleton1.hP + "/" + skeleton1.maxHp + ", SP: " + skeleton1.strikeP + ", DP: " + skeleton1.defendP, 310, 620);
        }

        if (isOnTheSameField(skeleton2) && skeleton2.isAlive() && myHero.isAlive()){
            graphics.drawString("Skeleton (level" + skeleton2.level +  "):  HP: " + skeleton2.hP + "/" + skeleton2.maxHp + ", SP: " + skeleton2.strikeP + ", DP: " + skeleton2.defendP, 310, 620);
        }

        if (isOnTheSameField(skeleton3) && skeleton3.isAlive() && myHero.isAlive()){
            graphics.drawString("Skeleton (level" + skeleton3.level +  "):  HP: " + skeleton3.hP + "/" + skeleton3.maxHp + ", SP: " + skeleton3.strikeP + ", DP: " + skeleton3.defendP, 310, 620);
        }

        repaint();

    }
}
