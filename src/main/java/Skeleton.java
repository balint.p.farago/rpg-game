public class Skeleton extends Monster{

    public Skeleton(int posX, int posY, int level) {
        super("skeleton.png", posX, posY);
        this.level = level;
        maxHp = hP = 2 * level * randomizeD6(6);
        defendP = level / 2 * randomizeD6(6);
        strikeP = level * randomizeD6(6);
    }


}
