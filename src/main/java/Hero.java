import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class Hero extends Character{

    public Hero(int posX, int posY, int level) {
        super("hero-down.png", posX, posY);
        this.level = level;
        maxHp = hP = 20 + 3 * randomizeD6(6) + (level - 1) * randomizeD6(6);
        defendP = 2 * randomizeD6(6);
        strikeP = 5 * randomizeD6(6);
    }


    public void move (int plusX, int plusY, int board[][]){

        int newPosX = plusX + this.posX;
        int newPosY = plusY + this.posY;
//To test the hero dont leave the table
        if (10 > newPosX && newPosX > -1 && 10 > newPosY && newPosY > -1){
//To test ther is not wall on the destination place
            if (board[newPosY][newPosX] == 1){
                this.posX = newPosX;
                this.posY = newPosY;
            }
        }
    }

    public void setImage (String filename){
        try {
            image = ImageIO.read(new File(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
