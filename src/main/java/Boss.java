public class Boss extends Monster{

    public Boss(int posX, int posY, int level) {
        super("boss.png", posX, posY);
        this.level = level;
        maxHp = hP = 2 * level * randomizeD6(6) + randomizeD6(6);
        defendP = level / 2 * randomizeD6(6) + randomizeD6(6) / 2;
        strikeP = level * randomizeD6(6) + level;
    }
}
