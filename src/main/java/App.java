import javax.swing.*;
import java.awt.*;

public class App{
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setPreferredSize(new Dimension(600,680));

        Board board = new Board();

        frame.add(board);
        frame.addKeyListener(board);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setFocusable(true);
    }
}
